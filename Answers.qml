import QtQuick 2.15
import QtQuick.Controls

Button {
    id: control
    text: qsTr("Button")
    width: parentalrectangle.width/2-10
    height: parentalrectangle.height/2.2
    property string myBackground: " "
    property string myText: " "
    font.bold: true
    font.pixelSize: parent.width/30
    font.family: "algerian"

    contentItem: Text {
        text: qsTr(myText)
        wrapMode: "WordWrap"
        width: parent.width
        onTextChanged: width = Math.min(parent.width, paintedWidth)
        font: control.font
        opacity: enabled ? 1.0 : 0.3
        color: control.down ? "#black" : "#green"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }
    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 40
        opacity: enabled ? 1 : 0.3
        color: control.hovered ? "black" : myBackground
        border.width: 3
        radius: 2
    }
}
