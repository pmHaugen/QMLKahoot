import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    id: root
    width: 640
    height: 480
    visible: true
    title: qsTr("Kahoot")
    Rectangle{
        id: questionsrect
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width
        height: parent.height/2
        color: "#2997b3"
        Text{
            id: questionText
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("What is the largest known dinosaur?")
            font.pixelSize: parent.width/25
            wrapMode: "WordWrap"
            width: parent.width-300
            onTextChanged: width = Math.min(parent.width, paintedWidth)
            font.family: "algerian"
        }
    }
    Text{
        anchors.left: parent.left
        text: "Username"
        font.pixelSize: 25
        font.family: "algerian"
    }
    Text{
        anchors.right: parent.right
        text: qsTr("Score: 13000")
        font.pixelSize: 25
        font.family: "algerian"
    }
    Rectangle{
        id: parentalrectangle
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width
        height: parent.height/2
        color: "black"
        Grid{
            id: grid
            rows: 2
            columns: 2
            anchors.centerIn: parent
            spacing: 10

            Answers { myBackground: "#6156c4"; myText: "Maraapunisaurus" }
            Answers { myBackground: "#c45656"; myText: "Patagotitan"     }
            Answers { myBackground: "#61c456"; myText: "Supersaurus"     }
            Answers { myBackground: "#c4c156"; myText: "Titanosaurus!"   }
        }
        Text{
            anchors.bottom: grid.top
            padding: 20
            anchors.horizontalCenter: grid.horizontalCenter
            text: "10s"
            font.pixelSize: 20
            font.family: "algerian"
        }
    }
}
